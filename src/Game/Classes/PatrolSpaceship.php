<?php


namespace BinaryStudioAcademy\Game\Classes;


class PatrolSpaceship extends Ship
{
    protected int $minStrength = 3;
    protected int $maxStrength = 4;
    protected int $minArmor = 2;
    protected int $maxArmor = 4;
    protected int $minLuck = 3;
    protected int $maxLuck = 6;
    protected int $strength = 0;
    protected int $armor = 0;
    protected int $luck = 0;
    protected int $health = 100;
    protected array $hold = ['green'];

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getArmor(): int
    {
        return $this->armor;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

    public function setStrength($strength): void
    {
        $this->strength = $strength;
    }

    public function setArmor($armor): void
    {
        $this->armor = $armor;
    }

    public function setHealth($health): void
    {
        $this->health = $health;
    }

    public function setHold($hold): void
    {
        $this->hold = $hold;
    }

}
