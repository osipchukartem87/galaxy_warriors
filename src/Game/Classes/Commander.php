<?php


namespace BinaryStudioAcademy\Game\Classes;


class Commander
{
    public const EXIT_COMMAND = 'exit';
    public const HELP_COMMAND = 'help';
    public const ATTACK_COMMAND = 'attack';

    private $allowedCommands = [
        self::HELP_COMMAND,
        self::ATTACK_COMMAND,
        self::EXIT_COMMAND
    ];


    public function help(): string
    {
        return 'List of commands:' . PHP_EOL
            .- 'help - shows this list of commands' . PHP_EOL
            .- 'stats - shows stats of spaceship' . PHP_EOL
            .- 'set-galaxy <home|andromeda|spiral|pegasus|shiar|xeno|isop> - provides jump into specified galaxy' . PHP_EOL
            .- 'attack - attacks enemy\'s spaceship' . PHP_EOL
            .- 'grab - grab useful load from the spaceship' . PHP_EOL
            .- 'buy <strength|armor|reactor> - buys skill or reactor (1 item)' . PHP_EOL
            .- 'apply-reactor - apply magnet reactor to increase spaceship health level on 20 points' . PHP_EOL
            .- 'whereami - shows current galaxy' . PHP_EOL
            .- 'restart - restarts game' . PHP_EOL
            .- 'exit - ends the game' . PHP_EOL
            .+'This method runs program step by step.'.PHP_EOL;
    }

    public function attack(Ship $playerShip, Ship $enemyShip)
    {
        // some logic
        return 'attack is end';
    }

}