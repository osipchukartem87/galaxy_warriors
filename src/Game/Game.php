<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Classes\Commander;
use BinaryStudioAcademy\Game\Classes\Galaxy;
use BinaryStudioAcademy\Game\Classes\PatrolSpaceship;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class Game
{
    private $random;
    private $commander;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->commander = new Commander();
    }

    public function start(Reader $reader, Writer $writer)
    {
//        $writer->writeln('Your goal is to develop a game "Galaxy Warriors".');
//        $writer->writeln('This method starts infinite loop with game logic.');
//        $writer->writeln('Use proposed implementation in order to tests work correct.');
//        $writer->writeln('Random float number: ' . $this->random->get());
//        $writer->writeln('Feel free to remove this lines and write yours instead.');
//        $writer->writeln('Press enter to start... ');

        for($i = 0; ; $i++) {
            $command = trim($reader->read());
            if ($command === Commander::EXIT_COMMAND) {
                break;
            }

            switch ($command) {
                case Commander::HELP_COMMAND:
                    $writer->writeln($this->commander->$command());
                    break;
                case Commander::ATTACK_COMMAND:
                    $writer->writeln($this->commander->$command(new PatrolSpaceship(), (new  Galaxy('ssss'))->getShip()));
                    break;
            }

        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $writer->writeln('This method runs program step by step.');
    }
}
